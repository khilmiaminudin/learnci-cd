import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React With CI/CD
        </a>
        <p>
          Belajar CI/CD dengan Gitlab dan Heroku
        </p>
        <a href="https://gitlab.com/khilmiaminudin/learnci-cd/-/branches/active">
          link repo GitLab
        </a>
        <p>
          Happy Eid Mubarrak
        </p>
      </header>
    </div>
  );
}

export default App;
